<?php

namespace Optimy\Exam\Services;

class DB
{
    private $pdo;

    private static $instance = null;

    private function __construct()
    {
        $dbname = $_ENV['DATABASE_NAME'];
        $host = $_ENV['DATABASE_HOST'];
        $dsn = "mysql:dbname={$dbname};host={$host}";
        $user = $_ENV['DATABASE_USER'];
        $password = $_ENV['DATABASE_PASSWORD'];

        $this->pdo = new \PDO($dsn, $user, $password);
    }

    public static function getInstance()
    {
        if (null === self::$instance) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }

    public function select($sql)
    {
        $sth = $this->pdo->query($sql);
        return $sth->fetchAll();
    }

    public function exec($sql)
    {
        return $this->pdo->exec($sql);
    }

    public function lastInsertId()
    {
        return $this->pdo->lastInsertId();
    }
}