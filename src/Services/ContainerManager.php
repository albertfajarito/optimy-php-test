<?php

namespace Optimy\Exam\Services;

class ContainerManager
{
    private static $container = null;

    public static function getContainer()
    {
        return self::$container;
    }

    public static function setContainer($container)
    {
        self::$container = $container;
    }
}