<?php

namespace Optimy\Exam\Commands;

use Optimy\Exam\Services\CommentManager;
use Optimy\Exam\Services\NewsManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'news-feed', description: 'Shows news and comments')]
class NewsFeed extends Command
{
    public function __construct(
        private NewsManager $newsManager,
        private CommentManager $commentManager
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            foreach ($this->newsManager->listNews() as $news) {
                $output->writeln("############ NEWS " . $news->getTitle() . " ############\n");
                $output->writeln($news->getBody() . "\n");
                foreach ($this->commentManager->listComments() as $comment) {
                    if ($comment->getNewsId() == $news->getId()) {
                        $output->writeln("Comment " . $comment->getId() . " : " . $comment->getBody() . "\n");
                    }
                }
            }
        } catch(Exception $exception) {
            $output->writeln($exception->getMessage());
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}