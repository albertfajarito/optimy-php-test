# Albert Fajarito PHP test

## 1. Installation

  - Create an empty database named `phptest` on your MySQL server
  - Create a new user `engineer` with password `secret`. I do not like the idea of using root anywhere.
  - Import the `dbdump.sql` to `phptest` database.
  - Copy `.env.example` to `.env` and populate database values
  - Run `composer install`


## 2. How To Use

In command line / terminal type `php bin/application news-feed`