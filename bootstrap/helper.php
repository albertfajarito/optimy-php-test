<?php

use Optimy\Exam\Services\ContainerManager;
use Symfony\Component\DependencyInjection\ContainerBuilder;

if (function_exists('app') === false) {
    function app(): ContainerBuilder
    {
        return ContainerManager::getContainer();
    }
}
