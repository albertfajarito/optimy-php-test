<?php

require_once 'vendor/autoload.php';

use Optimy\Exam\Services\ContainerManager;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

$container = new ContainerBuilder();
$loader = new XmlFileLoader($container, new FileLocator(dirname(__DIR__) . '/config/'));
$loader->load('Container.xml');
ContainerManager::setContainer($container);
