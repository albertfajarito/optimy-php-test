<?php

require_once 'vendor/autoload.php';
require_once 'bootstrap/dotenv.php';
require_once 'bootstrap/helper.php';
require_once 'bootstrap/container.php';